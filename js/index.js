function handleSizeImage() {
  let img_leftside = document.querySelector("#img-leftside");
  let img_size = document.querySelectorAll(".img-size");
  img_size.forEach((item) =>
    item.classList.add(`max-h-[${img_leftside.clientWidth / 3 - 10}px]`)
  );
}
$(window).on("resize", handleSizeImage);
handleSizeImage();
